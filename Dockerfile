FROM debian:stable-slim
ARG VERSION=5.0.2
ARG PREFIX=/opt/rt5
ARG RT_PLUGINS="RT::Extension::MergeUsers"

RUN apt-get update && \
    apt-get install -qy libapache2-mod-auth-mellon libapache2-mod-fcgid \
    apache2 cpanminus wget make spawn-fcgi libapache-session-perl \
    libbusiness-hours-perl libcgi-psgi-perl libcgi-emulate-psgi-perl \
    libcss-minifier-xs-perl libcss-squish-perl libdbd-mysql-perl \
    libclone-perl libconvert-color-perl libcrypt-eksblowfish-perl \
    libcrypt-x509-perl libstring-shellquote-perl libfile-which-perl \
    libnamespace-autoclean-perl libxml-rss-perl libweb-machine-perl \
    libuniversal-require-perl libtree-simple-perl libtime-parsedate-perl \
    libtext-wrapper-perl libtext-worddiff-perl libdata-guid-perl \
    libdata-ical-perl libdata-page-pageset-perl libdate-extract-perl \
    libdate-manip-perl libdatetime-format-natural-perl \
    libemail-address-perl libemail-address-list-perl libencode-hanextra-perl \
    libhtml-formatexternal-perl libhtml-formattext-withlinks-perl \
    libhtml-formattext-withlinks-andtables-perl libhtml-gumbo-perl \
    libhtml-mason-perl libhtml-mason-psgihandler-perl libhtml-quoted-perl \
    libhtml-rewriteattributes-perl libhtml-scrubber-perl libipc-run3-perl \
    libjson-perl libjavascript-minifier-xs-perl liblist-moreutils-perl \
    liblocale-maketext-fuzzy-perl liblocale-maketext-lexicon-perl \
    libmodule-path-perl libmodule-versions-report-perl libmoose-perl \
    libmoosex-nonmoose-perl libmoosex-role-parameterized-perl \
    libnet-cidr-perl libnet-ip-perl libregexp-common-perl \
    libregexp-common-net-cidr-perl libregexp-ipv6-perl librole-basic-perl \
    libscope-upper-perl libsymbol-global-name-perl libterm-readkey-perl \
    libtext-password-pronounceable-perl libtext-quoted-perl \
    libtext-template-perl libtext-wikiformat-perl libencode-detect-perl \
    libmime-tools-perl libnet-ldap-perl libparallel-prefork-perl \
    libwant-perl libdbd-sqlite3-perl libdbix-dbschema-perl \
    libcache-simple-timedexpiry-perl libmodule-build-tiny-perl \
    libextutils-installpaths-perl libextutils-config-perl \
    libextutils-helpers-perl msmtp-mta cron fetchmail patch && \
    wget -q https://download.bestpractical.com/pub/rt/release/rt-${VERSION}.tar.gz -O - | tar zx -C /tmp && \
    cd /tmp/rt-${VERSION} && \
    ./configure --prefix=${PREFIX} --disable-gpg --enable-externalauth && \
    RT_FIX_DEPS_CMD="cpanm --notest" make fixdeps && make install && \
    make fixperms && \
    chown www-data: ${PREFIX}/etc/RT_SiteConfig.pm ${PREFIX}/etc/schema.* \
    ${PREFIX}/etc/acl.* ${PREFIX}/etc/initialdata && a2enmod auth_mellon && \
    echo "*/5 * * * * /opt/rt5/sbin/rt-ldapimport --import >/var/log/ldapimport.log 2>&1" | crontab

RUN for plugin in ${RT_PLUGINS}; do PERL_MM_USE_DEFAULT=1 cpan ${RT_PLUGINS}; done && rm -rf /root/.cpan

WORKDIR /
ADD patches /patches
RUN for patch_file in $(ls /patches); do patch -p0 < /patches/${patch_file}; done

WORKDIR ${PREFIX}
ADD apache-default /etc/apache2/sites-enabled/000-default.conf
ADD entrypoint.sh /entrypoint.sh

CMD /entrypoint.sh
